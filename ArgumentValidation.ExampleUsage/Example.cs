﻿using System;

namespace ArgumentValidation.ExampleUsage
{
    public class Example
    {
        private int _count;
        private readonly DateTime _start;

        public Example(int count, DateTime start)
        {
            _count = Validate.Argument(() => count).Satisfies(i => i > 0 && i < 18).Value;
            _start = Validate.Argument(() => start).Satisfies(d => d > DateTime.UtcNow).Value;
        }

        public void Add(int i)
        {
            Validate.Argument(() => i).Satisfies(arg => arg > 0);

            // now i can be used
            _count += i;
        }

        public DateTime EndDateTime
        {
            get { return _start.AddDays(_count); }
        }
    }
}
