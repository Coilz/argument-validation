﻿using System;
using System.Diagnostics;

namespace ArgumentValidation
{
    [DebuggerStepThrough]
    public class Argument<TParameter>
    {
        private readonly Func<TParameter> _parameterFunction;
        private readonly Lazy<string> _parameterName;
        private readonly Lazy<TParameter> _parameterValue;

        internal Argument(Func<TParameter> parameterFunction)
        {
            _parameterFunction = parameterFunction;
            _parameterName = new Lazy<string>(() => GetName(parameterFunction));
            _parameterValue = new Lazy<TParameter>(parameterFunction);
        }

        internal Argument(Argument<TParameter> argument)
            : this(argument._parameterFunction)
        {
        }

        internal string Name
        {
            get { return _parameterName.Value; }
        }

        public TParameter Value
        {
            get { return _parameterValue.Value; }
        }

        private static string GetName(Func<TParameter> parameterFunction)
        {
            var fieldInfo = NaïveFieldInfoReader<TParameter>.GetFieldInfo(parameterFunction);

            return fieldInfo == null ? "*name*" : fieldInfo.Name;
        }
    }
}
