using System;
using System.Diagnostics;

namespace ArgumentValidation
{
    [DebuggerStepThrough]
    public static class Validate
    {
        public static Argument<TParameter> Argument<TParameter>(Func<TParameter> parameterFunction)
        {
            if (parameterFunction == null)
            {
                throw new ArgumentNullException("parameterFunction");
            }

            return new Argument<TParameter>(parameterFunction);
        }
    }
}
