using System;
using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;

namespace ArgumentValidation
{
    internal static class FieldInfoReader<TParameter>
    {
        // ReSharper disable once StaticFieldInGenericType
        private static readonly Lazy<OpCode[]> SingleByteOpCodes = new Lazy<OpCode[]>(LoadOpCodes);

        public static FieldInfo GetFieldInfo(Func<TParameter> parameterFunction)
        {
            var methodBodyIlByteArray = GetMethodBodyIlByteArray(parameterFunction);
            var fieldToken = GetFieldToken(methodBodyIlByteArray);

            return GetFieldInfo(parameterFunction, fieldToken);
        }

        private static byte[] GetMethodBodyIlByteArray(Func<TParameter> arg)
        {
            var methodBody = arg.Method.GetMethodBody();

            if (methodBody == null)
            {
                throw new InvalidOperationException();
            }

            return methodBody.GetILAsByteArray();
        }

        private static FieldInfo GetFieldInfo(Func<TParameter> parameterFunction, int fieldToken)
        {
            if (fieldToken <= 0) return null;

            var argType = parameterFunction.Target.GetType();
            var genericTypeArguments = GetSubclassGenericTypes(argType);
            var genericMethodArguments = parameterFunction.Method.GetGenericArguments();

            return argType.Module.ResolveField(fieldToken, genericTypeArguments, genericMethodArguments);
        }

        private static OpCode[] LoadOpCodes()
        {
            var singleByteOpCodes = new OpCode[0x100];

            var opcodeFieldInfos = typeof(OpCodes).GetFields();

            foreach (var info1 in opcodeFieldInfos)
            {
                if (info1.FieldType != typeof(OpCode)) continue;

                var singleByteOpCode = (OpCode)info1.GetValue(null);
                var singleByteOpcodeIndex = (ushort)singleByteOpCode.Value;

                if (singleByteOpcodeIndex < 0x100)
                {
                    singleByteOpCodes[singleByteOpcodeIndex] = singleByteOpCode;
                }
            }

            return singleByteOpCodes;
        }

        private static OpCode GetOpCode(byte[] methodBodyIlByteArray, ref int currentPosition)
        {
            ushort value = methodBodyIlByteArray[currentPosition++];

            return value != 0xfe ? SingleByteOpCodes.Value[value] : OpCodes.Nop;
        }

        private static int GetFieldToken(byte[] methodBodyIlByteArray)
        {
            var position = 0;

            while (position < methodBodyIlByteArray.Length)
            {
                var code = GetOpCode(methodBodyIlByteArray, ref position);

                if (code.OperandType == OperandType.InlineField)
                {
                    return ReadInt32(methodBodyIlByteArray, ref position);
                }

                position = MoveToNextPosition(position, code);
            }

            return 0;
        }

        private static int MoveToNextPosition(int position, OpCode code)
        {
            switch (code.OperandType)
            {
                case OperandType.InlineNone:
                    break;

                case OperandType.InlineI8:
                case OperandType.InlineR:
                    position += 8;
                    break;

                case OperandType.InlineField:
                case OperandType.InlineBrTarget:
                case OperandType.InlineMethod:
                case OperandType.InlineSig:
                case OperandType.InlineTok:
                case OperandType.InlineType:
                case OperandType.InlineI:
                case OperandType.InlineString:
                case OperandType.InlineSwitch:
                case OperandType.ShortInlineR:
                    position += 4;
                    break;

                case OperandType.InlineVar:
                    position += 2;
                    break;

                case OperandType.ShortInlineBrTarget:
                case OperandType.ShortInlineI:
                case OperandType.ShortInlineVar:
                    position++;
                    break;

                default:
                    throw new InvalidOperationException("Unknown operand type.");
            }
            return position;
        }

        private static int ReadInt32(byte[] il, ref int position)
        {
            return ((il[position++] | (il[position++] << 8)) | (il[position++] << 0x10)) | (il[position++] << 0x18);
        }

        private static Type[] GetSubclassGenericTypes(Type toCheck)
        {
            var genericArgumentsTypes = new List<Type>();

            while (toCheck != null)
            {
                if (toCheck.IsGenericType)
                {
                    genericArgumentsTypes.AddRange(toCheck.GetGenericArguments());
                }

                toCheck = toCheck.BaseType;
            }

            return genericArgumentsTypes.ToArray();
        }
    }
}
