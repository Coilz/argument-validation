﻿using System.Diagnostics;

namespace ArgumentValidation
{
    [DebuggerStepThrough]
    public class ReferenceTypeArgument<TParameter> : Argument<TParameter>
    {
        internal ReferenceTypeArgument(Argument<TParameter> argument)
            : base(argument)
        {
        }
    }
}
