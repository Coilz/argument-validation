using System;
using System.Reflection;

namespace ArgumentValidation
{
    internal static class Na´veFieldInfoReader<TParameter>
    {
        public static FieldInfo GetFieldInfo(Func<TParameter> arg)
        {
            var target = arg.Target;
            if (target == null) return null;

            var fields = target.GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
            return fields.Length == 1 ? fields[0] : null;
        }
    }
}
