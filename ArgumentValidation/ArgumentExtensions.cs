﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Linq.Expressions;

namespace ArgumentValidation
{
    [DebuggerStepThrough]
    public static class ArgumentExtensions
    {
        public static ReferenceTypeArgument<TParameter> IsNotNull<TParameter>(this Argument<TParameter> argument) where TParameter : class
        {
            if (argument == null)
            {
                throw new ArgumentNullException("argument", "Argument cannot be null.");
            }

            if (argument.Value == null)
            {
                throw new ArgumentNullException(argument.Name, "Parameter cannot be null.");
            }

            return new ReferenceTypeArgument<TParameter>(argument);
        }

        public static ReferenceTypeArgument<string> IsNotEmpty(this ReferenceTypeArgument<string> argument)
        {
            if (argument.Value.Length == 0)
            {
                throw new ArgumentException("Parameter cannot be an empty string.", argument.Name);
            }

            return new ReferenceTypeArgument<string>(argument);
        }

        public static Argument<TParameter> Satisfies<TParameter>(this Argument<TParameter> argument, Expression<Func<TParameter, bool>> expression)
        {
            argument.ApplyValidation(
                expression.Compile(),
                () => string.Format("The parameter '{0}' failed the following validation '{1}'", argument.Name, expression));

            return argument;
        }

        public static Argument<TParameter> Satisfies<TParameter>(this Argument<TParameter> argument, Func<TParameter, bool> function, string messageFormat, params object[] messageParameters)
        {
            argument.ApplyValidation(function, () => string.Format(CultureInfo.InvariantCulture, messageFormat, messageParameters));

            return argument;
        }

        private static void ApplyValidation<TParameter>(this Argument<TParameter> argument, Func<TParameter, bool> testFunction, Func<string> messageFunction)
        {
            if (!testFunction.Invoke(argument.Value))
            {
                throw new ArgumentException(messageFunction.Invoke(), argument.Name);
            }
        }
    }
}
