﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Runtime.InteropServices;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ArgumentValidation.UnitTests
{
    [TestClass]
    public class ValidateTests
    {
        [TestMethod, ExpectedException(typeof(ArgumentNullException))]
        public void Validate_WhenTheParameterFunctionIsNull_ThenAnArgumentNullExceptionIsThrown()
        {
            Validate.Argument((Func<int>)null);
        }

        [TestMethod, ExpectedException(typeof(ArgumentNullException))]
        public void Validate_WhenTheParameterFunctionReturnsNull_ThenAnArgumentNullExceptionIsThrownWithoutParameterName()
        {
            try
            {
                Validate.Argument(() => (object)null).IsNotNull();
            }
            catch (ArgumentNullException ex)
            {
                Assert.AreEqual("*name*", ex.ParamName);
                throw;
            }
        }

        [TestMethod]
        public void Validate_WhenTheParameterFunctionReturnsAValidString_ThenTheValueCanBeRetrievedFromTheArgument()
        {
            var value = Validate.Argument(() => "someString").IsNotNull().IsNotEmpty().Value;
            Assert.AreEqual("someString", value);
        }

        [TestMethod, ExpectedException(typeof(ArgumentException))]
        public void Validate_WhenTheParameterFunctionReturnsAnInvalidString_ThenAnArgumentExceptionIsThrown()
        {
            try
            {
                Validate.Argument(() => "someString").IsNotNull().IsNotEmpty().Satisfies(s => s.Length > 100);
            }
            catch (ArgumentException ex)
            {
                Assert.AreEqual("*name*", ex.ParamName);
                throw;
            }
        }

        [TestMethod, ExpectedException(typeof(ArgumentException))]
        public void Validate_WhenAnEmptyStringIsTestedToBeNotEmpty_ThenAnArgumentExceptionIsThrown()
        {
            var a = string.Empty;
            Validate.Argument(() => a).IsNotNull().IsNotEmpty();
        }

        [TestMethod, ExpectedException(typeof(ArgumentException))]
        public void Validate_WhenAShortStringIsTestedToBeOfLengthGreaterThan100_ThenAnArgumentExceptionIsThrown()
        {
            var a = "aString";
            Validate.Argument(() => a).IsNotNull().IsNotEmpty().Satisfies(s => s.Length > 100);
        }

        [TestMethod, ExpectedException(typeof(ArgumentException))]
        public void Validate_WhenAShortStringIsTestedToBeOfLengthGreaterThan100WithAGivenMessage_ThenAnArgumentExceptionIsThrownWithThatMessage()
        {
            var a = "aString";
            var messageFormat = "The string {0} is too short";

            try
            {
                Validate.Argument(() => a).IsNotNull().IsNotEmpty().Satisfies(s => s.Length > 100, messageFormat, a);
            }
            catch (ArgumentException ex)
            {
                Assert.IsTrue(ex.Message.Contains(string.Format(CultureInfo.InvariantCulture, messageFormat, a)));
                throw;
            }
        }

        [TestMethod, ExpectedException(typeof(ArgumentNullException))]
        public void Validate_WhenANullStringIsTestedToBeNotEmpty_ThenAnArgumentNullExceptionIsThrownWithTheNameOfTheArgument()
        {
            try
            {
                string a = null;
                Validate.Argument(() => a).IsNotNull().IsNotEmpty();
            }
            catch (ArgumentNullException ex)
            {
                Assert.AreEqual("a", ex.ParamName);
                throw;
            }
        }

        [TestMethod, ExpectedException(typeof(ArgumentNullException))]
        public void Validate_WhenATwoStringsAreTestedToBeNotEmpty_ThenArgumentNullExceptionsAreThrownWithTheNameOfTheArgument()
        {
            try
            {
                string a = null;
                Validate.Argument(() => a).IsNotNull().IsNotEmpty();
            }
            catch (ArgumentNullException ex)
            {
                Assert.AreEqual("a", ex.ParamName);
            }

            try
            {
                string b = null;
                Validate.Argument(() => b).IsNotNull().IsNotEmpty();
            }
            catch (ArgumentNullException ex)
            {
                Assert.AreEqual("b", ex.ParamName);
                throw;
            }
        }

        [TestMethod]
        public void Validate_WhenAStringSatifiesAnExpression_ThenNoExceptionIsThrown()
        {
            var a = string.Empty;
            Validate.Argument(() => a).Satisfies(string.IsNullOrEmpty, "The string must be null or empty.");

            Assert.IsTrue(true);
        }

        [TestMethod, ExpectedException(typeof(ArgumentException))]
        public void Validate_WhenASubjectIsNotCompleteAndTestedToBeComplete_ThenAnArgumentExceptionIsThrownWithTheNameOfTheArgument()
        {
            try
            {
                var subject = new Subject<StreamReader>();
                Validate.Argument(() => subject).IsNotNull().Satisfies(s => s.IsComplete);
            }
            catch (ArgumentException ex)
            {
                Assert.AreEqual("subject", ex.ParamName);
                throw;
            }
        }

        [TestMethod]
        public void Validate_WhenASubjectHasAValidNameAndTestedToHaveAValidName_ThenNoExceptionIsThrown()
        {
            const string aValidName = "aValidName";

            var subject = new Subject<List<int>>  { Name = aValidName };
            Validate.Argument(() => subject).Satisfies(s => s.Name == aValidName && !s.IsComplete);

            Assert.IsTrue(true);
        }

        [TestMethod, ExpectedException(typeof(ArgumentException))]
        public void Validate_WhenTheNameOfTheSubjectIsPassedToArgument_ThenAnArgumentExceptionIsThrownWithTheNameOfTheSubject()
        {
            try
            {
                const string aValidName = "aValidName";

                var subject = new Subject<DateTime> { Name = aValidName };
                Validate.Argument(() => subject.Name).Satisfies(s => s != aValidName);
            }
            catch (ArgumentException ex)
            {
                Assert.AreEqual("subject", ex.ParamName);
                throw;
            }
        }

        [TestMethod, ExpectedException(typeof(ArgumentException))]
        public void Validate_WhenAMethodBodyIsPassedToArgument_ThenAnExceptionIsThrown()
        {
            try
            {
                const string aValidName = "aValidName";

                SubjectBase subjectWithName = new Subject<long> { Name = aValidName };
                Validate.Argument(() =>
                {
                    const string a = ".";
                    return a + subjectWithName.Name;
                }).Satisfies(s => s == aValidName);
            }
            catch (ArgumentException ex)
            {
                Assert.AreEqual("subjectWithName", ex.ParamName);
                throw;
            }
        }

        class Subject<T> : SubjectBase
        {
            public bool IsComplete { get; set; }
            public T Value { get; set; }
        }

        abstract class SubjectBase
        {
            public string Name { get; set; }
        }
    }
}
