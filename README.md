# README #

The code in this repository has been derived from a post of [Bronumski](http://stackoverflow.com/users/227200/bronumski) that I found [here](http://bronumski.blogspot.nl/2010/06/taking-pain-out-of-parameter-validation.html). Since I think it's usefull, I decided to create an online repo for it.

### What is this repository for? ###
This repository contains logic for the validation of parameters that can be used via a fluent interface.

### How do I get set up? ###
For now there is no Nuget package for this repo, so you'll have to copy the code or include the project in your solution. You won't need any dependencies to other 3rd-party libraries.

### Contribution guidelines ###

* Create a fork of this repo
* make your changes
* write unit tests that cover your changes
* create a pull request for the changes

### Who do I talk to? ###

* [me on BitBucket](https://bitbucket.org/Coilz)
